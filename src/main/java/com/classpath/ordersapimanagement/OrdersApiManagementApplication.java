package com.classpath.ordersapimanagement;

import com.classpath.ordersapimanagement.controller.OrderController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import java.util.List;

@SpringBootApplication
@EnableWebSecurity
public class OrdersApiManagementApplication {
  public static void main(String[] args) {
        SpringApplication.run(OrdersApiManagementApplication.class, args);
    }

}
