package com.classpath.ordersapimanagement.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppHealthIndicator implements HealthIndicator {
    @Override
    public Health health() {
        /*
         1. you defined what you application is ready means to you.
         2. Keep it lightweight
         3. Dont put authentication and auth for this endpoint
         4. Should not take more then few milliseconds to complete
         */

        return Health.up().withDetail("Service-A", "Service is Up").build();
    }
}