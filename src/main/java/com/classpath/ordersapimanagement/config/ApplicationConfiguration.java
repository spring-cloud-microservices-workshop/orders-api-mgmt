package com.classpath.ordersapimanagement.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Configuration
@Slf4j
public class ApplicationConfiguration {

    @Bean
    public RestTemplate restTemplate () {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setInterceptors(List.of(new CustomClientInterceptor()));
        return restTemplate;
    }
}

@Slf4j
class CustomClientInterceptor implements ClientHttpRequestInterceptor {
    @Override
    public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] bytes, ClientHttpRequestExecution clientHttpRequestExecution) throws IOException {
        log.info("Came inside the intercept method :: ");
        httpRequest.getHeaders().add("Authorization", UUID.randomUUID().toString());
        return clientHttpRequestExecution.execute(httpRequest, bytes);
    }
}