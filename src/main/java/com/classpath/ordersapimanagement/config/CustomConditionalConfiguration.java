package com.classpath.ordersapimanagement.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnJava;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.system.JavaVersion;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomConditionalConfiguration {

    @ConditionalOnProperty( prefix = "app", name = "flag", havingValue = "true", matchIfMissing = true)
    @Bean("customUser")
    public User user(){
        return new User();
    }

    @ConditionalOnMissingBean(name="customUser")
    @Bean
    public User userBasedOnClass (){
        return  new User();
    }

    @ConditionalOnJava(JavaVersion.EIGHT)
    @Bean
    public User userBasedOnJVM8(){
        return new User();
    }

    @ConditionalOnJava(JavaVersion.ELEVEN)
    @Bean
    public User userBasedOnJVM11(){
        return new User();
    }
}


class User {

}