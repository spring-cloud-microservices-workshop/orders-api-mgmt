package com.classpath.ordersapimanagement.controller;

import com.classpath.ordersapimanagement.model.Order;
import com.classpath.ordersapimanagement.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/orders")
public class OrderController {

    private ApplicationContext applicationContext;

    private OrderService orderService;

    @GetMapping
    public Set<Order> fetchAll() {
        return this.orderService.fetchAll();
    }

    @GetMapping("/{id}")
    public Order fetchOrderById(@PathVariable("id") long orderId) {
        return this.orderService
                .fetchOrderById(orderId);
    }

    @PostMapping
    public Order saveOrder(@Valid @RequestBody Order order ){
       return this.orderService.save(order);
    }

    @GetMapping("/beans")
    public List<String> beans() {
        return List.of(this.applicationContext.getBeanDefinitionNames());
    }

    @GetMapping("/name/{name}")
    public Set<Order> fetchByCustomerName(@PathVariable("name") String name){
        return this.orderService.fetchAllByCustomerName(name);
    }
}