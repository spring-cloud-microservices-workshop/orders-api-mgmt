package com.classpath.ordersapimanagement.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/api/v1/todos")
@AllArgsConstructor
public class TodoController {

    private RestTemplate restTemplate;

    @GetMapping
    public String fetchTodos(){
        String response = this.restTemplate.getForObject("https://jsonplaceholder.typicode.com/todos", String.class);
        return response;
    }
}