package com.classpath.ordersapimanagement.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.eclipse.jetty.http.HttpStatus.BAD_REQUEST_400;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleInvalidOrders(Throwable exception) {
        return  ResponseEntity.status(BAD_REQUEST_400).body(exception.getMessage());
    }
}