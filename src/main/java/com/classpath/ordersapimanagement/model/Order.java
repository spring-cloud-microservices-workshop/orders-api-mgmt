package com.classpath.ordersapimanagement.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;

@Setter
@Getter
@ToString
@EqualsAndHashCode(of = "orderId")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name="orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long orderId;

    @Column(name = "name", nullable = false, unique = true)
    @NotEmpty(message = "Customer name cannot be empty")
    private String customerName;

    @Min(value = 15000, message = "Order price should be greater than 15.000")
    @Max(value = 150000, message = "Order price should be less than 1.50.000")
    private double orderPrice;

    private LocalDate orderCreated;
}