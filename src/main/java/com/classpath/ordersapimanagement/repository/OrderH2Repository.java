package com.classpath.ordersapimanagement.repository;


import com.classpath.ordersapimanagement.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface OrderH2Repository extends JpaRepository<Order, Long> {

    Set<Order> findOrdersByCustomerName(String customerName);
}