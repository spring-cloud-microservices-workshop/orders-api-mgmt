package com.classpath.ordersapimanagement.repository;

import com.classpath.ordersapimanagement.model.Order;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

//@Repository
public class OrderRepository {
    private Set<Order> ordersSet = new HashSet<>(Arrays.asList(
            Order.builder().orderId(111).orderPrice(25_000).orderCreated(LocalDate.now()).customerName("Suresh").build(),
            Order.builder().orderId(112).orderPrice(45_000).orderCreated(LocalDate.now()).customerName("Ramesh").build(),
            Order.builder().orderId(113).orderPrice(66_000).orderCreated(LocalDate.now()).customerName("Vishwa").build(),
            Order.builder().orderId(114).orderPrice(35_000).orderCreated(LocalDate.now()).customerName("Raman").build(),
            Order.builder().orderId(115).orderPrice(75_000).orderCreated(LocalDate.now()).customerName("Priya").build()

    ));

    private Set<Order> orders = Set.of(
            Order.builder().orderId(111).orderPrice(25_000).orderCreated(LocalDate.now()).customerName("Suresh").build(),
            Order.builder().orderId(112).orderPrice(45_000).orderCreated(LocalDate.now()).customerName("Ramesh").build(),
            Order.builder().orderId(113).orderPrice(66_000).orderCreated(LocalDate.now()).customerName("Vishwa").build(),
            Order.builder().orderId(114).orderPrice(35_000).orderCreated(LocalDate.now()).customerName("Raman").build(),
            Order.builder().orderId(116).orderPrice(85_000).orderCreated(LocalDate.now()).customerName("Usha").build(),
            Order.builder().orderId(115).orderPrice(75_000).orderCreated(LocalDate.now()).customerName("Priya").build()
    );
    public Set<Order> fetchAll(){
        return this.orders;
    }

    public Optional<Order> fetchOrderById(long orderId) {
        return this.orders.stream()
                           .filter(order -> order.getOrderId() == orderId)
                           .findFirst();
    }

}