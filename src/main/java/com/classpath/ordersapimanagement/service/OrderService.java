package com.classpath.ordersapimanagement.service;

import com.classpath.ordersapimanagement.model.Order;

import java.util.Set;

public interface OrderService {

     Order save(Order order);

     Set<Order> fetchAll();

     Order fetchOrderById(long orderId);

     void deleteOrderById(long orderId);

     Set<Order> fetchAllByCustomerName(String name);
}