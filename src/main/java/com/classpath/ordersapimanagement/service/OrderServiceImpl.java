package com.classpath.ordersapimanagement.service;

import com.classpath.ordersapimanagement.model.Order;
import com.classpath.ordersapimanagement.repository.OrderH2Repository;
import com.classpath.ordersapimanagement.repository.OrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {

    private OrderH2Repository orderRepository;

    @Override
    public Order save(Order order) {
        return this.orderRepository.save(order);
    }

    @Override
    public Set<Order> fetchAll() {
        return new HashSet<>(this.orderRepository.findAll());
    }

    @Override
    public Order fetchOrderById(long orderId) {
        return this.orderRepository
                .findById(orderId)
                .orElseThrow(() -> new IllegalArgumentException(" Order with the given orderId is not present"));
    }

    @Override
    public void deleteOrderById(long orderId) {

    }

    @Override
    public Set<Order> fetchAllByCustomerName(String name) {
        return this.orderRepository.findOrdersByCustomerName(name);
    }
}