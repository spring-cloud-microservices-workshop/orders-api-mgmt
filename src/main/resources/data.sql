-- noinspection SqlDialectInspectionForFile

-- noinspection SqlNoDataSourceInspectionForFile

insert into user (user_id, email_address, password, username) values (1, 'kiran@gmail.com', '$2a$10$Fp8Nfkoz6WfAmxlSDXoZ3uspMd0F3PRG4wfrFKsVCFzhDrfRUcQDG', 'kiran');
insert into user (user_id, email_address, password, username) values (2, 'vinay@gmail.com', '$2a$10$FwNwDs7bXbJmF1IR/.1r8uAjLBMQoqCBDls6hbygnN3Uj7He6jRDa', 'vinay');

insert into role (role_id, role_name) values (1, 'ROLE_USER');
insert into role (role_id, role_name) values (2, 'ROLE_ADMIN');

insert into user_roles(user_id, role_id) values (1, 1),(2, 1), (2,2);
